#!/usr/bin/python
import sys
import Adafruit_DHT
import requests
import requests
from pprint import pprint
import json
import csv
import datetime
import time

def get_weather():
    # Here we fetch the data from openweathermap api. Please use your own API-key
    r = requests.get('http://api.openweathermap.org/data/2.5/weather?q=Espoo&APPID=YOUR_API_KEY')
    return r.json()

def write_csv(data):
    # After each measurement, the data is saved to a csv file
    with open('data.csv', 'a') as f:
        writer = csv.writer(f, delimiter=",", lineterminator="\n")
        writer.writerow(data)



while True:
    print "Measuring..."
    sensor_humidity, sensor_temperature = Adafruit_DHT.read_retry(11, 4)

    try:
        weather_dict = get_weather()
        temperature = round(float(weather_dict['main']['temp']) - 273, 1)   
        humidity = round(float(weather_dict['main']['humidity']), 1)

        print "API: Temp:", temperature, "Humidity:", humidity
        print "SENSOR: Temp:", sensor_temperature, "Humidity:", sensor_humidity

        # Getting the time variables
        d = datetime.date.today()
        month = d.month
        day = d.day
        a = datetime.datetime.now().time()
        day_time = round(a.hour + a.minute / 60.0 + 3.0, 1)

        write_csv([month, day, day_time, sensor_humidity, sensor_temperature, temperature])
    except:
        print "Error fetching temperature data from the API"
    time.sleep(60)
