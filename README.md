# Temperature Prediction without Outside Thermometer

The idea in this hobby project is to predict outside temperature without thermometer outside. 
By utilizing machine learning and inside temperature and humidity measurements, we can predict the outside temperature.
Of course, we could just use an outside thermometer - but where's the fun in that:D

## Hardware Components

- Raspberry Pi 3
- [DHT22 temperature-humidity sensor](https://www.adafruit.com/product/385)

## Software Components

- scikit-learn
- Openweathermap API
- Adafruit library for the sensor

## Files included
- 'get_measurements.py' does data gathering
- 'model_training.ipynb' train the machine learing model
- 'data.csv' has some data that I gathered

## Results

We managed to predict the outside temperature with a mean squared error of ~3 celsius degrees. This was possible 
using k-NN Regression. The results would have been different, if the regressor didn't utilize time of the day and month to predict the temperature. However, in the second figure that shows correlation analysis of the data - some 
correlation exists between sensor measurements and outside temperature.


<img src="src/images/graph.png"  width="600" height="300">
<br>
<i>Cross-validation of the regression model.</i>
<br>
<br>
<img src="src/images/data_correlation.JPG"  width="600" height="162">
<br>
<i>Correlation analysis on the data.</i>



## Next steps

I will add more details on how to use the project yourself. In addition, I will make a script that utilizes the trained machine learning model.